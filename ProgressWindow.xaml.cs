﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileSearch
{
    /// <summary>
    /// Interaction logic for ProgressWindow.xaml
    /// </summary>
    public partial class ProgressWindow : Window, INotifyPropertyChanged
    {
        #region Fields

        readonly Progress _progress;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new progress window.
        /// </summary>
        /// <remarks>
        /// This constructor should only be accessed by the Utilities class.
        /// </remarks>
        /// <param name="msg">The message to display in the window.</param>
        internal ProgressWindow(string msg)
        {
            InitializeComponent();
            _progBar.IsIndeterminate = true;
            _tbMsg.Text = msg;
        }

        /// <summary>
        /// Creates a new progress window.
        /// </summary>
        /// <param name="progress">The object used to update the progress bar.</param>
        public ProgressWindow(Progress progress)
        {
            InitializeComponent();
            this.DataContext = _progress = progress;

            BindingExpression binding = BindingOperations.GetBindingExpression(_progBar, ProgressBar.IsIndeterminateProperty);
            binding.UpdateTarget();

            binding = BindingOperations.GetBindingExpression(_tbMsg, TextBlock.TextProperty);
            binding.UpdateTarget();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the PropertyChanged event for the property with the specified name.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        private void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
