﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using FileSearch.FP;
using Microsoft.Win32;

namespace FileSearch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += this.MainWindow_Loaded;
        }


        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(_tbSearchString.Text))
            {
                MessageBox.Show("No search string entered.", "Invalid Operation", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (string.IsNullOrEmpty(_tbRoot.Text))
            {
                MessageBox.Show("Root path is not set.", "Invalid Operation", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string searchStr = _tbSearchString.Text;
            string root = _tbRoot.Text;
            string[] fileExts = this.GetFileExtensions();
            bool fileNamesOnly = (_cbFileNames.IsChecked != null) ? _cbFileNames.IsChecked.Value : false;

            var progress = new Progress(isIndeterminate: true) { Message = "Searching for '" + _tbRoot.Text + "'..." };
            var progWin = new ProgressWindow(progress) { Owner = this };
            progWin.Show();

            Task<List<MatchInfo>> task = Task.Run(() => this.Search(searchStr, root, fileExts, fileNamesOnly, progress));
            task.ContinueWith(t =>
            {
                progWin.Close();

                if (t.Exception != null)
                    MessageBox.Show(t.Exception.InnerException.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                else
                {
                    _lbMatches.ItemsSource = t.Result;
                    _tbCount.Text = t.Result.Count.ToString();
                }
                
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }


        private void ButtonSetRoot_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                Description = "Select root folder to search",
                RootFolder = Environment.SpecialFolder.MyComputer
            };

            if (!string.IsNullOrEmpty(_tbRoot.Text))
                dialog.SelectedPath = _tbRoot.Text;

            dialog.ShowDialog();

            if (!string.IsNullOrEmpty(dialog.SelectedPath))
                _tbRoot.Text = dialog.SelectedPath;
        }


        private IEnumerable<MatchInfo> FindInFile(string searchString, string file)
        {
            string contents = null;
            try
            {
                contents = File.ReadAllText(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot read file: " + file + ". " + ex.Message, "Read Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            MatchCollection matches = !string.IsNullOrEmpty(contents) ? Regex.Matches(contents, searchString) : null;
            return this.ToMatchInfos(matches, file);
        }

        private IEnumerable<MatchInfo> FindInFileName(string searchString, string file) => this.ToMatchInfos(Regex.Matches(file, searchString), file);

        private string[] GetFileExtensions()
        {
            if (string.IsNullOrEmpty(_tbFileExt.Text))
                return new string[0];

            string[] fileExts = _tbFileExt.Text.Split(';').Select(s => s.Trim()).ToArray();
            return fileExts;
        }

        private string GetSerializationPath()
        {
            string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Settings.xml");
            return path;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= this.MainWindow_Loaded;

            string path = this.GetSerializationPath();
            if (File.Exists(path))
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    var serializer = new XmlSerializer(typeof(Settings));
                    var settings = (Settings)serializer.Deserialize(stream);

                    _tbFileExt.Text = settings.FileExtensions ?? string.Empty;
                    _tbRoot.Text = settings.Root ?? string.Empty;
                    _tbSearchString.Text = settings.SearchString ?? string.Empty;
                    _cbFileNames.IsChecked = settings.SearchFileNamesOnly;
                }
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            var settings = new Settings()
            {
                FileExtensions = _tbFileExt.Text,
                Root = _tbRoot.Text,
                SearchFileNamesOnly = (_cbFileNames.IsChecked != null) ? _cbFileNames.IsChecked.Value : false,
                SearchString = _tbSearchString.Text
            };

            string path = this.GetSerializationPath();
            using (var stream = new FileStream(path, FileMode.Create))
            {
                var serializer = new XmlSerializer(typeof(Settings));
                serializer.Serialize(stream, settings);
            }

            base.OnClosing(e);
        }

        private List<MatchInfo> Search(string searchString, string dir, string[] fileExts, bool fileNamesOnly, Progress progress)
        {
            var matchInfos = new List<MatchInfo>();

            if (!Directory.Exists(dir))
                MessageBox.Show("Directory does not exist: " + dir, "Directory Not Found", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                this.SearchImpl(searchString, dir, matchInfos, fileExts, fileNamesOnly, progress);

            return matchInfos;
        }

        private void SearchImpl(string searchString, string dir, List<MatchInfo> matchInfos, string[] fileExts, bool fileNamesOnly, Progress progress)
        {
            string[] files = Funcs.Try<string[], Exception>(() => Directory.GetFiles(dir), ex => new string[0]);
            foreach (string file in files)
            {
                progress.Message = "Searching: " + file;

                if (fileExts.Length > 0 && !fileExts.Contains(System.IO.Path.GetExtension(file)))
                    continue;

                IEnumerable<MatchInfo> infos = fileNamesOnly ? this.FindInFileName(searchString, file) : this.FindInFile(searchString, file);
                matchInfos.AddRange(infos);
            }

            string[] subDirs = Funcs.Try<string[], Exception>(() => Directory.GetDirectories(dir), ex => new string[0]);
            foreach (string subDir in subDirs)
            {
                this.SearchImpl(searchString, subDir, matchInfos, fileExts, fileNamesOnly, progress);
            }
        }

        private IEnumerable<MatchInfo> ToMatchInfos(MatchCollection matches, string file)
        {
            var matchInfos = new List<MatchInfo>();
            if (matches != null && matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var info = new MatchInfo(file, match);
                    matchInfos.Add(info);
                }
            }

            return matchInfos;
        }
    }
}
