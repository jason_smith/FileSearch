﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileSearch
{
    public class MatchInfo : PropertyChangedNotifier
    {
        readonly Match _match;
        readonly string _file;

        public MatchInfo(string file, Match match)
        {
            _file = file;
            _match = match;
        }


        public string File
        {
            get { return _file; }
        }


        public int Index
        {
            get { return _match.Index; }
        }


        public Match Match
        {
            get { return _match; }
        }
    }
}
