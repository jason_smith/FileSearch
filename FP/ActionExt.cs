﻿using System;
using System.Collections.Generic;
using System.Text;
using Unit = System.ValueTuple;

namespace FileSearch.FP
{
    public static class ActionExt
    {
        public static Func<Unit> ToFunc(this Action action) 
            => () => { action(); return default; };

        public static Func<T, Unit> ToFunc<T>(this Action<T> action) 
            => t => { action(t); return default; };

        public static Func<T1, T2, Unit> ToFunc<T1, T2>(this Action<T1, T2> action) 
            => (t1, t2) => { action(t1, t2); return default; };

        public static Func<T1, T2, T3, Unit> ToFunc<T1, T2, T3>(this Action<T1, T2, T3> action)
            => (t1, t2, t3) => { action(t1, t2, t3); return default; };

        public static Func<T1, T2, T3, T4, Unit> ToFunc<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action)
            => (t1, t2, t3, t4) => { action(t1, t2, t3, t4); return default; };

        public static Func<T1, T2, T3, T4, T5, Unit> ToFunc<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> action)
            => (t1, t2, t3, t4, t5) => { action(t1, t2, t3, t4, t5); return default; };
    }
}
