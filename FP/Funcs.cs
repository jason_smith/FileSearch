﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unit = System.ValueTuple;

namespace FileSearch.FP
{
    public static class Funcs
    {
        public static Task<T> Async<T>(T t) => Task.FromResult(t);

        public static Task ExecuteAsync(Action action, CancellationToken? token = null) 
            => ExecuteAsync(action.ToFunc(), token);

        public static Task ExecuteAsync<T>(Func<T> func, CancellationToken? token = null) 
            => Task.Run(() => func(), token ?? new CancellationTokenSource().Token);

        public static Task ExecuteAsync(Action action, Action cleanUp, CancellationToken? token = null) 
            => ExecuteAsync(action.ToFunc(), cleanUp, token);

        public async static Task<T> ExecuteAsync<T>(Func<T> func, Action cleanUp, CancellationToken? token)
            => await Finally(() => Task.Run(() => func(), token ?? new CancellationTokenSource().Token), () => cleanUp?.Invoke());

        public static Task<T> ExecuteAsync<T>(Func<Task<T>> func, Action cleanUp = null)
            => Finally(async () => await func(), () => cleanUp?.Invoke());

        public static void Finally(Action action, Action cleanUp) => Finally(action.ToFunc(), cleanUp);

        public static T Finally<T>(Func<T> func, Action cleanUp)
        {
            try { return func(); }
            finally { cleanUp(); }
        }

        public async static Task Finally(Func<Task> func, Action cleanUp)
        {
            try { await func(); }
            finally { cleanUp(); }
        }

        public async static Task<T> Finally<T>(Func<Task<T>> func, Action cleanUp)
        {
            try { return await func(); }
            finally { cleanUp(); }
        }

        public static void Try(Action action) => Try(action, ex => { });

        public static void Try(Action action, Action<Exception> catchAction) => Try(action.ToFunc(), catchAction);

        public static void Try<Ex>(Action action, Action<Ex> catchAct) where Ex : Exception => Try(action.ToFunc(), catchAct);

        public static T Try<T, Ex>(Func<T> func, Action<Ex> catchAction) where Ex : Exception => Try(func, (Ex e) => { catchAction(e); return default(T); });

        public static T Try<T, Ex>(Func<T> func, Func<Ex, T> catchFunc) where Ex : Exception
        {
            try { return func(); }
            catch (Ex e) { return catchFunc(e); }
        }

        public static T Unique<T>(Func<IEnumerable<T>> getExisting, Func<T, T, bool> compare, Func<T, T> update, T desired)
        {
            IEnumerable<T> existing = getExisting();

            bool isUnique = false;
            T uniqueT = desired;

            while (!isUnique)
            {
                isUnique = true;
                foreach (T item in existing)
                {
                    if (compare(item, uniqueT))
                    {
                        isUnique = false;
                        uniqueT = update(uniqueT);
                        break;
                    }
                }
            }

            return uniqueT;
        }

        public static decimal UniqueDecimal(Func<IEnumerable<decimal>> getExisting, decimal desiredNum)
            => Unique(getExisting, (x, y) => x == y, x => x + 1, desiredNum);

        public static double UniqueDouble(Func<IEnumerable<double>> getExisting, double desiredNum)
            => Unique(getExisting, (x, y) => x == y, x => x + 1, desiredNum);

        public static int UniqueInt(Func<IEnumerable<int>> getExisting, int desiredNum)
            => Unique(getExisting, (x, y) => x == y, x => x + 1, desiredNum);

        public static long UniqueLong(Func<IEnumerable<long>> getExisting, long desiredNum)
            => Unique(getExisting, (x, y) => x == y, x => x + 1, desiredNum);

        public static uint UniqueUInt(Func<IEnumerable<uint>> getExisting, uint desiredNum)
            => Unique(getExisting, (x, y) => x == y, x => x + 1, desiredNum);

        public static string UniqueString(Func<IEnumerable<string>> getExisting, string desiredName)
        {
            int appendNum = 2;
            return Unique(getExisting, (s1, s2) => string.Equals(s1, s2, StringComparison.Ordinal), s => $"{desiredName} ({appendNum++})", desiredName);
        }

        public static Unit Unit() => default;

        public static TRet Using<TDisp, TRet>(Func<TDisp> dispGenerator, Func<TDisp, TRet> usingBody) where TDisp : IDisposable
        {
            using (TDisp disp = dispGenerator.Invoke())
            {
                return usingBody.Invoke(disp);
            }
        }
    }
}
