﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace FileSearch
{
    public abstract class PropertyChangedNotifier : INotifyPropertyChanged
    {
        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected PropertyChangedNotifier()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Determines whether an exception is thrown or a call to Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the PropertyChanged event for the specified property.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.VerifyPropertyName(propertyName);

            var handler = this.PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Verifies that the specified property name actually exists on the current object.
        /// </summary>
        /// <param name="propertyName">The name of the property to verify.</param>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        private void VerifyPropertyName(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    System.Diagnostics.Debug.Fail(msg);
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
