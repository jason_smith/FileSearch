﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSearch
{
    public class Settings
    {
        public string FileExtensions { get; set; }
        public string Root { get; set; }
        public bool SearchFileNamesOnly { get; set; }
        public string SearchString { get; set; }
    }
}
