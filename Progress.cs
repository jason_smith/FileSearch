﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FileSearch
{
    public class Progress : PropertyChangedNotifier, IProgress<int>
    {
        #region Fields

        string _msg;
        int _total;
        int _pctComplete;
        bool _isIndeterminate;

        #endregion

        #region Events

        public event EventHandler<int> ProgressChanged;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new Progress object that supplies progress updates to the ProgressWindow.
        /// </summary>
        /// <param name="total">The total amount of items, tasks, etc.</param>
        public Progress(int total = 1, bool isIndeterminate = false)
        {
            _total = (total > 1) ? total : 1;
            _isIndeterminate = isIndeterminate;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether the progress can be determined.
        /// </summary>
        public bool IsIndeterminate
        {
            get { return _isIndeterminate; }
            set
            {
                if (_isIndeterminate != value)
                {
                    _isIndeterminate = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the message to display in the progress window.
        /// </summary>
        public string Message
        {
            get { return _msg; }
            set
            {
                if (_msg != value)
                {
                    _msg = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the current progress percentage.
        /// </summary>
        public int PercentageComplete
        {
            get { return _pctComplete; }
            private set
            {
                if (_pctComplete != value)
                {
                    _pctComplete = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the total amount of items, tasks, etc.
        /// </summary>
        public int Total
        {
            get { return _total; }
            set
            {
                if (_total != value)
                {
                    _total = (value > 1) ? value : 1; // total must be greater than zero to avoid dividing by zero
                    this.OnPropertyChanged();
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reports a progress update.
        /// </summary>
        /// <param name="value">The value of the updated progress.</param>
        public void Report(int value)
        {
            this.PercentageComplete = (int)((double)value / _total * 100);

            var handler = this.ProgressChanged;
            if (handler != null)
                handler.Invoke(this, this.PercentageComplete);
        }

        #endregion
    }
}
